import 'package:flutter/material.dart';
import 'dart:ui';

class FirstButton extends StatelessWidget {
  const FirstButton({
    Key? key,
    required this.height,
    required this.width,
  }) : super(key: key);

  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: height * 0.02083),
        Center(
          child: GestureDetector(
            child: Padding(
              padding: EdgeInsets.fromLTRB(width * 0.02963, height * 0.0125,
                  width * 0.02963, height * 0.0125),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(width * 0.02593),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(
                        sigmaX: width * 0.0083,
                        sigmaY: width * 0.0083,
                      ),
                      child: Center(
                        child: Container(
                          width: width * 0.05185,
                          height: width * 0.05185,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                              width: width * 0.000462963,
                              color: Color.fromRGBO(255, 255, 255, 0.25),
                            ),
                            color: Colors.white.withOpacity(0.4),
                          ),
                          child: Center(
                            child: SizedBox(
                              width: width * 0.02963,
                              child: Image.asset(
                                "assets/img/ico_home_64px.png",
                                fit: BoxFit.fitWidth,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: width * 0.0111),
                  Text(
                    '처음으로',
                    style: TextStyle(
                      fontFamily: 'notoSansKR',
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                      fontSize: width * 0.02593,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
