import 'package:flutter/material.dart';
import 'package:kiosk/controllers/video_state_controller.dart';
import 'package:get/get.dart';

class BackgroundStoppedImage extends StatelessWidget {
  const BackgroundStoppedImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: size.width,
      child: Image.asset(
        Get.put(VideoStateController()).videoUrl=="assets/video/hello.mp4"?'assets/img/background_center.png':'assets/img/background_left.png',
        fit: BoxFit.fill,
      ),
    );
  }
}