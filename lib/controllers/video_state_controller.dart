import 'package:async/async.dart';
import 'package:get/get.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';

class VideoStateController extends GetxController {
  String _videoUrl = "";
  VideoPlayerController? _controller;

  VideoPlayerController? get controller => _controller;

  String get videoUrl => _videoUrl;

  Future<void> changeUrl(String url) async {
    _videoUrl = url;
    if (_controller == null) {
      _initControllers(url);
    } else {
      if (url == "assets/video/hello.mp4") {
        _controller!.addListener(() {
          if (_controller != null) {
            if (_controller!.value.position == _controller!.value.duration) {
              update();
              final oldController = _controller;
              WidgetsBinding.instance!.addPostFrameCallback((_) async {
                await oldController!.dispose();
                _initControllers(url);
              });
              _controller = null;
              update();
            }
          }
        });
      }
        else{
          final oldController = _controller;
          WidgetsBinding.instance!.addPostFrameCallback((_) async {
            await oldController!.dispose();
            _initControllers(url);
          });
          _controller = null;
          update();
        }
      }
    }


  void _initControllers(String url) async {
    _videoUrl = url;
    AsyncMemoizer finishVideo = AsyncMemoizer<void>();
    _controller = VideoPlayerController.asset(url)
      ..setLooping(url=="assets/video/wait_left.mp4"||url=="assets/video/wait_center.mp4")
      ..initialize().whenComplete(() {
        _controller!.play();
        update();
      })
      ..addListener(() {
        if(_controller != null && url == "assets/video/hello.mp4"){
          if ( _controller!.value.position >= _controller!.value.duration - Duration(milliseconds: 500)) {
            finishVideo.runOnce(() {
              changeUrl("assets/video/wait_center.mp4");
            });
          }
        }
      });
  }
}
