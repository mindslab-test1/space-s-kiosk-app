import 'package:get/get.dart';
import 'package:kiosk/screens/splash_screen.dart';

final List<GetPage> routes = [
  GetPage<SplashScreen>(
      name: "/SplashScreen", page: () => const SplashScreen()),
];
