import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kiosk/widgets/avatar_video_player.dart';
import 'package:kiosk/widgets/background_stopped_image.dart';
import '../widgets/menus/choice_menu.dart';
import '../widgets/current_time.dart';
import '../controllers/port_state_controller.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  void initState() {
    Get.put(PortStateController()).getPorts();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    Get.put(PortStateController()).connectTo(null);
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Stack(
            children: [
              BackgroundStoppedImage(),
              AvatarVideoPlayer(),
              Positioned(
                top: height * 0.02083,
                left: width * 0.03704,
                child: SizedBox(
                  height: height * 0.02765625,
                  width: width * 0.1490741,
                  child: SvgPicture.asset("assets/img/signatureLogo.svg"),
                ),
              ),
              Positioned(
                right: width * 0.03704,
                top: height * 0.0174479167,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: const [
                    CurrentTime(),
                  ],
                ),
              ),
              GetBuilder<PortStateController>(
                  init: PortStateController(),
                  builder: (controller) => controller.isDetected?ChoiceMenu():Container()
              )
            ],
          ),
        ),
      ),
    );
  }
}
